Created By FiyCsf For Use With Halo 5 Weapon Models in Unity.

This should play nicely with all Halo 5 Weapons Textures etc, and is written using the amplify shader editor and is clearly commented should anyone wish to edit it.

This shader was Made with the Amplify Shader Editor for users to make changes as they see fit, while giving users a quick way to get a Weapon mode in unity.

It is Designed to work with the Halo 5 Colour, Control and Normal Map Files for the weapons specifically.

**There is a Discord for update information and general work related to kitbashing Halo Models:**

Current invite link: https://discord.gg/TkycpmwD2R 

This work is licensed under the Creative Commons Attribution-NonCommercial 3.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/3.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
